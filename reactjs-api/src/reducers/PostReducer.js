const initialState = {
  columns: [
    { title: "Tiêu đề", field: "name" },
    { title: "Tác giả", field: "author" },
    { title: "Chuyên mục", field: "category" },
    { title: "Thẻ", field: "tag" },
    { title: "Bình luận", field: "comment" },
    { title: "Thời gian", field: "date" }
  ],
  posts: []
};

export default function(state = initialState, action) {
  switch (action.type) {
    case "GET_POSTS":
      return { ...state, posts: action.posts };
    default:
      return state;
  }
}
