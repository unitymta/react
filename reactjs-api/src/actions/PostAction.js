export function getPosts(posts) {
  return {
    type: "GET_POSTS",
    posts
  };
}
