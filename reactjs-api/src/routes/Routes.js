import React, { Component } from "react";
import { Route } from "react-router-dom";
import Post from "../components/pages/Post/Post";
import PostCategory from "../components/pages/PostCategory/PostCategory";
import PostAdd from "../components/pages/PostAdd/PostAdd";
import PostTag from "../components/pages/PostTag/PostTag";
import User from "../components/pages/User/User";
import Login from "../components/pages/Login/index";

class Routes extends Component {
  render() {
    let name = "tokenAuth";
    var value = "; " + document.cookie;
    var parts = value.split("; " + name + "=");
    let tokenAuth = parts
      .pop()
      .split(";")
      .shift();
    console.log(tokenAuth, "kkklllll___");
    let loginCheck;
    if (tokenAuth != null) {
      console.log('11')
      loginCheck = true;
    } else {
      console.log('222')
      loginCheck = false;
    }
    return (
      <div className="wrapper">
        <Route path="/user" exact component={User} />

        <Route path="/post" exact component={Post} />
        <Route path="/post/list" exact component={Post} />
        <Route path="/post/category" exact component={PostCategory} />
        <Route path="/post/add" exact component={PostAdd} />
        <Route path="/post/tag" exact component={PostTag} />
        <Route path="/login" exact component={Login} />
      </div>
    );
  }
}

export default Routes;
