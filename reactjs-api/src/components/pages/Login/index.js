import React, { Component } from "react";
import TextField from "@material-ui/core/TextField";
import ButtonGroup from "@material-ui/core/ButtonGroup";
import Button from "@material-ui/core/Button";
import * as Config from "../../../constants/Config";
import "./login.css";

class index extends Component {
  constructor(props) {
    super(props);
    this.state = { username: "", password: "" };
  }
  async handleLogin() {
    console.log(this.state.username, "___", this.state.password);
    let urlAuth = Config.URL_AUTH;
    const data = {
      username: this.state.username,
      password: this.state.password
    };
    // fetch(urlAuth).then(result => console.log(result));
    const response = await fetch(urlAuth, {
      method: "POST", // *GET, POST, PUT, DELETE, etc.
      mode: "cors", // no-cors, *cors, same-origin
      cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
      credentials: "same-origin", // include, *same-origin, omit
      headers: {
        "Content-Type": "application/json"
        // 'Content-Type': 'application/x-www-form-urlencoded',
      },
      redirect: "follow", // manual, *follow, error
      referrer: "no-referrer", // no-referrer, *client
      body: JSON.stringify(data) // body data type must match "Content-Type" header
    });
    response.json().then(dt => {
      console.log(dt.token);
      if (dt.token != null) {
        document.cookie = `tokenAuth=${dt.token}`;
      }
    });
    //   console.log(response.json(),'response');
    //   return await response.json();
  }
  updateUsername(event) {
    this.setState({
      username: event.target.value
    });
  }
  updatePassword(event) {
    this.setState({
      password: event.target.value
    });
  }

  render() {   
    return (
      <form className="" noValidate autoComplete="off">
        <TextField
          id="outlined-username-input"
          label="Username"
          type="text"
          name="username"
          autoComplete="username"
          margin="normal"
          variant="outlined"
          value={this.state.username}
          onChange={e => this.updateUsername(e)}
        />
        <TextField
          id="outlined-password-input"
          label="Password"
          type="password"
          autoComplete="current-password"
          margin="normal"
          variant="outlined"
          value={this.state.password}
          onChange={e => this.updatePassword(e)}
        />
        <ButtonGroup
          variant="contained"
          color="primary"
          aria-label="full-width contained primary button group"
        >
          <Button onClick={() => this.handleLogin()}>Đăng nhập</Button>
        </ButtonGroup>
      </form>
    );
  }
}

export default index;
