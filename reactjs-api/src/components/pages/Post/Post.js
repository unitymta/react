import React, { Component } from "react";
import { connect } from "react-redux";
import MaterialTable from "material-table";
import PostContainer from "../../../containers/Post/PostContainer";
import { getPosts } from "../../../actions//PostAction";

class Post extends Component {
  constructor(props) {
    super(props);
    this.state = {
      posts: this.props.posts
    };
  }
  componentDidMount() {
    let posts = this.state.posts;
    PostContainer().then(data => {
      data.map(value => {
        let object = {
          id: value.id,
          name: value.title.rendered,
          author: "sangtx",
          category: "",
          tag: "",
          comment: "",
          date: value.modified
        };
        return this.setState({ posts: posts.push(object) });
      });
    });
  }

  componentWillUnmount() {
    let posts = [];
    const action = getPosts(posts);
    this.props.dispatch(action);
  }
  render() {
    return (
      <div>
        <MaterialTable
          title="Bài viết"
          columns={this.props.columns}
          data={this.props.posts}
          editable={{
            onRowAdd: newData =>
              new Promise(resolve => {
                setTimeout(() => {
                  resolve();
                  const data = [...this.props.posts];
                  data.push(newData);
                  this.setState({ ...this.state, data });
                }, 600);
              }),
            onRowUpdate: (newData, oldData) =>
              new Promise(resolve => {                
                setTimeout(() => {
                  console.log('12122',resolve(), newData, oldData);
                  resolve();
                  const data = [...this.props.posts];
                  data[data.indexOf(oldData)] = newData;
                  this.setState({ ...this.state, data });
                }, 600);
              }),
            onRowDelete: oldData =>
              new Promise(resolve => {
                setTimeout(() => {
                  resolve();
                  const data = [...this.state.data];
                  data.splice(data.indexOf(oldData), 1);
                  this.setState({ ...this.state, data });
                }, 600);
              })
          }}
        />
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    posts: state.PostReducer.posts,
    columns: state.PostReducer.columns
  };
}

export default connect(
  mapStateToProps,
  null
)(Post);
