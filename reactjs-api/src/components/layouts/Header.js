import React, { Component } from "react";
import Grid from "@material-ui/core/Grid";
import AddIcon from "@material-ui/icons/Add";
import HomeIcon from '@material-ui/icons/Home';

class Header extends Component {
  render() {
    return (
      <Grid className="header-container" container>
        <Grid className="header-wrap" item xs={12} sm={6}>
          <a className="header-link" href="http://reactjs.unitymta.com">
            <HomeIcon />
            reactjs.unitymta.com
          </a>
          <div className="header-add">
            <AddIcon />
            <div className="header-add__btn">Mới</div>
          </div>
        </Grid>
        <Grid className="header-profile" item xs={12} sm={6}>
			<ul className="header-profile__list">
				<li className="header-profile__item">
					<a className="header-profile__link" href="/">Chào, <span className="display-name">sangtx</span>
						<img alt="avatar" src="http://2.gravatar.com/avatar/b8aef47da44f0f381d1df49976acb8ae?s=26&d=mm&r=g" srcSet="" className="" height={26} width={26} />
					</a>
					<div className="header-profile__submenu">
						<ul className="submenu-list">
							<li className="submenu-item">
								<a className="header-profile__link" href="/">
									<img alt="avatar" src="http://2.gravatar.com/avatar/b8aef47da44f0f381d1df49976acb8ae?s=26&d=mm&r=g" srcSet="" className="avatar avatar-64 photo" height={64} width={64} />
									<span className="display-name">sangtx</span>
								</a>
							</li>
							<li className="submenu-item"><a className="header-profile__link" href="/">Chỉnh sửa thông tin</a></li>
							<li className="submenu-item"><a className="header-profile__link" href="/">Đăng xuất</a></li>
						</ul>
					</div>
				</li>
			</ul>
		</Grid>
      </Grid>
    );
  }
}

export default Header;
