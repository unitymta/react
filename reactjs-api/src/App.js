import React, { Component } from "react";
import { Provider } from "react-redux";
import { createStore } from "redux";
import { BrowserRouter as Router, Route } from "react-router-dom";
import RootReducer from "./reducers/RootReducer";
import Routes from "./routes/Routes";
import "./resources/styles/normalize.css";
import "./resources/styles/common.css";
import Header from "./components/layouts/Header";
import Sidebar from "./components/layouts/Sidebar";

class App extends Component {
  render() {
    const store = createStore(RootReducer);  
    return (
      <Provider store={store}>
        <Router>
          <div className="app">
            <Header />
            <Sidebar />
            <div className="content" id="content">
              <Routes />
            </div>
          </div>
        </Router>
      </Provider>
    );
  }
}

export default App;
