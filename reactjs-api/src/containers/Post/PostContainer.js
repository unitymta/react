import * as Config from "../../constants/Config";

const PostContainer = async function() {
  let url = Config.URL_API + "posts";
  let results = await fetch(url);
  return results.json();
};

export default PostContainer;
