import React, { Component } from "react";
import DashboardIcon from '@material-ui/icons/Dashboard';
import PostAddIcon from '@material-ui/icons/PostAdd';
import PhotoLibraryIcon from '@material-ui/icons/PhotoLibrary';
import PagesIcon from '@material-ui/icons/Pages';
import FeedbackIcon from '@material-ui/icons/Feedback';
import PersonIcon from '@material-ui/icons/Person';
import { NavLink } from "react-router-dom";
import ExpandLessIcon from '@material-ui/icons/ExpandLess';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';

class Sidebar extends Component {
  render() {
    return (
      <div className="sidebar">
        <ul className="sidebar-list">
            <li className="sidebar-item">
                <NavLink className="sidebar-link" to="#" exact activeClassName="active">
                    <span className="sidebar-link__icon"><DashboardIcon/></span><span className="sidebar-link__text">Bảng tin</span>
                </NavLink>
            </li>
            <li className="sidebar-item">
                <NavLink className="sidebar-link" to="/post" activeClassName="active">
                    <span className="sidebar-link__icon"><PostAddIcon/></span>
                    <span className="sidebar-link__text">Bài viết</span>
                    <span className="sidebar-arrow expendless"><ExpandLessIcon/></span>
                    <span className="sidebar-arrow expendmore"><ExpandMoreIcon/></span>
                </NavLink>
                <ul className="sub-sidebar__list">
                    <li className="sub-sidebar__item">
                        <NavLink className="sub-link" to="/post/list" exact activeClassName="active">Tất cả bài viết</NavLink>
                    </li>
                    <li className="sub-sidebar__item">
                        <NavLink className="sub-link" to="/post/add" exact activeClassName="active">Thêm bài viết</NavLink>
                    </li>
                    <li className="sub-sidebar__item">
                        <NavLink className="sub-link" to="/post/category" exact activeClassName="active">Chuyên mục</NavLink>
                    </li>
                    <li className="sub-sidebar__item">
                        <NavLink className="sub-link" to="/post/tag" exact activeClassName="active">Thẻ</NavLink>
                    </li>
                </ul>
            </li>
            <li className="sidebar-item">
                <NavLink className="sidebar-link" to="#" exact activeClassName="active">
                    <span className="sidebar-link__icon"><PhotoLibraryIcon/></span><span className="sidebar-link__text">Thư viện</span>
                </NavLink>
            </li>
            <li className="sidebar-item">
                <NavLink className="sidebar-link" to="#" exact activeClassName="active">
                    <span className="sidebar-link__icon"><PagesIcon/></span><span className="sidebar-link__text">Trang</span>
                </NavLink>
            </li>
            <li className="sidebar-item">
                <NavLink className="sidebar-link" to="#" exact activeClassName="active">
                    <span className="sidebar-link__icon"><FeedbackIcon/></span><span className="sidebar-link__text">Phản hồi</span>
                </NavLink>
            </li>
            <li className="sidebar-item">
                <NavLink className="sidebar-link" to="/user" exact activeClassName="active">
                    <span className="sidebar-link__icon"><PersonIcon/></span><span className="sidebar-link__text">Thành viên</span>
                </NavLink>
            </li>
        </ul>
      </div>
    );
  }
}

export default Sidebar;
