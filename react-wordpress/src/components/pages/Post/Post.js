import React, { Component } from "react";
import { connect } from "react-redux";
import MaterialTable from "material-table";
import PostContainer from "../../../containers/Post/PostContainer";

class Post extends Component {
  constructor(props) {
    super(props);
    this.state = {
      columns: [
        { title: "Tiêu đề", field: "name" },
        { title: "Tác giả", field: "author" },
		{ title: "Chuyên mục", field: "category" },
		{ title: "Thẻ", field: "tag" },
		{ title: "Bình luận", field: "comment" },
		{ title: "Thời gian", field: "date" }
      ],
      data: [
        { name: "", author: "", category: "", tag: "", comment: "", date: "" }
      ]
    };
  }
  componentDidMount(){
	//   let t = PostContainer();
	//   console.log(t,'DIS');
  }
  render() {
	  console.log(this.props.posts,'PSLssa1');
	  let posts = PostContainer();
	  console.log(posts,'sang22');
	  let p = posts.map( (index, value) => {
		return value;
	  });
	//   let p = posts.forEach(function(index, value){
	// 	return value;
	//   });
	  console.log(p,'1111'); 

    return (
      <div>        
        <MaterialTable
          title="Bài viết"
          columns={this.state.columns}
          data={this.state.data}
          editable={{
            onRowAdd: newData =>
              new Promise(resolve => {
                setTimeout(() => {
                  resolve();
                  const data = [...this.state.data];
                  data.push(newData);
                  this.setState({ ...this.state, data });
                }, 600);
              }),
            onRowUpdate: (newData, oldData) =>
              new Promise(resolve => {
                setTimeout(() => {
                  resolve();
                  const data = [...this.state.data];
                  data[data.indexOf(oldData)] = newData;
                  this.setState({ ...this.state, data });
                }, 600);
              }),
            onRowDelete: oldData =>
              new Promise(resolve => {
                setTimeout(() => {
                  resolve();
                  const data = [...this.state.data];
                  data.splice(data.indexOf(oldData), 1);
                  this.setState({ ...this.state, data });
                }, 600);
              })
          }}
        />
      </div>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return {
    getData: () => dispatch({ type: "getData" })
  };
}

function mapStateToProps(state) {
  return {
    posts: state.PostReducer.posts
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Post);
