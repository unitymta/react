import * as Config from "../../constants/Config";

function PostContainer() {
  let url = Config.URL_API + "posts";
  let posts = [];

  fetch(url)
    .then(results => {
      return results.json();
    })
    .then(data => {
      data.map(post => {
        return posts.push(post);
      });
    });
  return posts;
}

export default PostContainer;
