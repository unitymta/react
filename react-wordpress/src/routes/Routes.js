import React, { Component } from "react";
import { Route } from "react-router-dom";
import Post from "../components/pages/Post/Post";
import PostCategory from '../components/pages/PostCategory/PostCategory';
import PostAdd from '../components/pages/PostAdd/PostAdd';
import PostTag from '../components/pages/PostTag/PostTag';
import User from "../components/pages/User/User";

class Routes extends Component {
  render() {
    return (
      <div className="wrapper">
        <Route path="/user" exact component={User} />

        <Route path="/post" exact component={Post} />    
        <Route path="/post/list" exact component={Post} />
        <Route path="/post/category" exact component={PostCategory} />
		<Route path="/post/add" exact component={PostAdd} />
		<Route path="/post/tag" exact component={PostTag} />
      </div>
    );
  }
}

export default Routes;
